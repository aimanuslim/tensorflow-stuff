# Module 2 Basic Tensorflow Operations
# Eagle Mode Demo
# Author: Dr. Alfred Ang

import tensorflow as tf
tf.enable_eager_execution()

# a = tf.constant(1)
# b = tf.constant(2)
# print(a+b)
#
# a = tf.constant([
# 				[1,2,3],
# 				[4,5,6],
# 				[7,8,9]])
# print(a)
# print(a.numpy())